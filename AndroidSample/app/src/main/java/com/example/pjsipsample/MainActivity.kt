package com.example.pjsipsample

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import com.example.pjsipsample.sip.SipApp
import com.example.pjsipsample.sip.SipConnectionInfo
import org.pjsip.pjsua2.*
import org.pjsip.pjsua2.pj_constants_.PJ_TRUE
import org.pjsip.pjsua2.pjsua_call_flag.PJSUA_CALL_UPDATE_CONTACT

class MainActivity : AppCompatActivity(), SipApp.SipAppListener {
    private var phoneNumberEdTxt: EditText? = null
    private var makeCallBtn: Button? = null
    private var endCallBtn: Button? = null
    private var audioSkinSpinner: Spinner? = null
    private var currentSipCall: Call? = null
    private var currentSipAccount: Account? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        phoneNumberEdTxt = findViewById(R.id.phoneNumberEdTxt)
        makeCallBtn = findViewById<Button?>(R.id.makeCallBtn).apply {
            this.setOnClickListener { makeCall(phoneNumberEdTxt!!.text.toString()) }
        }
        endCallBtn = findViewById<Button?>(R.id.endCallBtn).apply {
            this.setOnClickListener { hangupCall() }
        }
        audioSkinSpinner = findViewById(R.id.audioSkinSpinner)
        SipApp.initialize(getSipConnection(), this)
        registerAccount()
    }

    private fun registerAccount() {
        if(currentSipAccount == null && SipApp.isInitialized){
            Log.i(TAG,"Phone.registerAccount(): validations & registering account")
            if(!havePermissions())
                requestRequiredPermissions()
            else
                Log.i(TAG,"Phone.registerAccount(): all dependencies properly started")
            try {
                val accCfg = AccountConfig()
                val sipConnection = getSipConnection()
                accCfg.idUri = "sip:${sipConnection.username}@${sipConnection.domain}"
                accCfg.regConfig.registrarUri = "sip:${sipConnection.sipHost}"
                accCfg.natConfig.iceEnabled = false
                accCfg.videoConfig.autoTransmitOutgoing = true
                accCfg.videoConfig.autoShowIncoming = true
                accCfg.natConfig.contactRewriteUse = PJ_TRUE
                accCfg.natConfig.contactRewriteMethod = PJ_TRUE
                accCfg.ipChangeConfig.hangupCalls = false
                accCfg.ipChangeConfig.reinviteFlags = PJSUA_CALL_UPDATE_CONTACT.toLong()
                val creds = accCfg.sipConfig.authCreds
                creds.clear()
                val authInfo = AuthCredInfo("Digest", "*", sipConnection.username, 0, sipConnection.password)
                creds.add(authInfo)
                val proxies = accCfg.sipConfig.proxies
                proxies.clear()
                proxies.add("sip:${sipConnection.sipHost}")
                currentSipAccount = Account()
                currentSipAccount!!.create(accCfg)
            } catch (e: Exception) {
                Log.e(TAG, "Error while registering account", e)
            }
        } else if(!SipApp.isInitialized){
            Log.e(TAG, "MainActivity.registerAccount(): SIP NOT Initialized, Attempting Register")
        } else {
            Log.e(TAG, "MainActivity.registerAccount(): Phone.registerAccount(): Can not register an already registered account")
        }
    }

    private fun makeCall(number: String?){
        if(currentSipCall == null && !number.isNullOrBlank() ){
            val domainId = getSipConnection().domain
            val destUri = "$<sip:$number@$domainId>"
            Log.i(TAG, "Phone.makeCall: Calling to $destUri")
            currentSipCall = Call(currentSipAccount)
            val prm = CallOpParam(true)
            try {
                currentSipCall!!.makeCall(destUri, prm)
            } catch (e: Exception) {
                if (currentSipCall != null) currentSipCall!!.delete()
                currentSipCall = null
                Log.e(TAG, "Error while trying to make a call to $number", e)
            }
        }
    }

    private fun hangupCall() {
        if(currentSipCall != null){
            val callState = CallState.valueFromPjsip(currentSipCall!!.info!!.state)
            val statusCode: Int = when(callState){
                CallState.INCOMING -> pjsip_status_code.PJSIP_SC_TEMPORARILY_UNAVAILABLE
                else ->  pjsip_status_code.PJSIP_SC_REQUEST_TERMINATED
            }
            val answerParam = CallOpParam().apply {
                this.statusCode = statusCode
            }
            val isEarlyState = arrayOf(CallState.INCOMING, CallState.EARLY).indexOf(callState) >= 0
            if(isEarlyState){
                currentSipCall!!.answer(answerParam)
            } else {
                currentSipCall!!.hangup(answerParam)
            }
        }
    }

    private fun getSipConnection(): SipConnectionInfo {
        return SipConnectionInfo(
            displayName=resources.getString(R.string.DISPLAY_NAME),
            domain=resources.getString(R.string.DOMAIN),
            username=resources.getString(R.string.USERNAME),
            password=resources.getString(R.string.PASSWORD),
            sipHost=resources.getString(R.string.SIP_HOST),
            sipPort=resources.getInteger(R.integer.SIP_PORT),
            sipDevicePort=resources.getInteger(R.integer.SIP_DEVICE_PORT).toLong(),
            stunServer=resources.getString(R.string.STUN_SERVER)
        )
    }

    private fun havePermissions(): Boolean = checkPermissions().map { entry -> entry.value }.reduce { acc, b -> acc && b }

    private fun checkPermissions(): HashMap<String, Boolean>{
        val micGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
        val readContactsGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
        val phoneCallGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
        val blueToothConnectGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED
        return hashMapOf(
            Manifest.permission.RECORD_AUDIO to micGranted,
            Manifest.permission.READ_CONTACTS to readContactsGranted,
            Manifest.permission.CALL_PHONE to phoneCallGranted,
            Manifest.permission.BLUETOOTH_CONNECT to blueToothConnectGranted,
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestRequiredPermissions() {
        if(havePermissions())
            return
        val permissionsToRequest = checkPermissions().filter { entry -> !entry.value }.map { entry -> entry.key }.toTypedArray()
        requestPermissions(permissionsToRequest, REQUEST_CODE)

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        val permissionsGranted = grantResults.isNotEmpty() && grantResults.map { granted -> granted == PackageManager.PERMISSION_GRANTED }.reduce { acc, b -> acc && b }
        if(requestCode == REQUEST_CODE && permissionsGranted){
            registerAccount()
        } else {
            requestRequiredPermissions()
        }
    }

    //region SipApp.SipAppListener
    override fun notifyRegState(code: Int, reason: String?, expiration: Int) {
        val statusCode = code / 100
        val success = ( statusCode == SUCCESS_CODE)
        val msgStr = "${if (expiration == 0) "Unregistration" else "Registration"} ${if (success) "successful" else " failed: $reason"}"
    }

    override fun notifyIncomingCall(call: Call?) {
        TODO("Not yet implemented")
    }

    override fun notifyCallState(sipCall: Call) {
        TODO("Not yet implemented")
    }

    override fun notifyCallMediaState(call: Call?) {
        TODO("Not yet implemented")
    }

    override fun onCallTransferStatus(prm: OnCallTransferStatusParam?) {
        TODO("Not yet implemented")
    }

    override fun onNatDetectionComplete(prm: OnNatDetectionCompleteParam?) {
        TODO("Not yet implemented")
    }

    override fun onMwiInfo(prm: OnMwiInfoParam?) {
        TODO("Not yet implemented")
    }

    override fun notifyDidFinishStaring(success: Boolean) {
        TODO("Not yet implemented")
    }

    override fun onIpChangeProgress(prm: OnIpChangeProgressParam?) {
        TODO("Not yet implemented")
    }

    //endregion

    companion object{
        const val TAG = "MAIN_ACTIVITY"
        const val SUCCESS_CODE = 2
        const val CLIENT_ERROR_CODE = 4
        const val SERVER_ERROR_CODE = 5
        const val SIP_NETWORK_ERROR_CODE = 6
        const val REQUEST_CODE = 200
    }

    enum class CallState{
        INCOMING{
            override fun toString(): String {
                return "Incoming"
            }
        },
        DISCONNECTED{
            override fun toString(): String {
                return "Disconnected"
            }
        },
        CALLING{
            override fun toString(): String {
                return "Disconnected"
            }
        },
        EARLY{
            override fun toString(): String {
                return "Early"
            }
        },
        CONNECTING{
            override fun toString(): String {
                return "Connecting"
            }
        },
        CONFIRMED{
            override fun toString(): String {
                return "Confirmed"
            }
        },
        UNKNOWN{
            override fun toString(): String {
                return "Unknown"
            }
        };

        companion object{
            fun valueFromPjsip(pjsipStateValue: Int): CallState {
                return when(pjsipStateValue){
                    pjsip_inv_state.PJSIP_INV_STATE_INCOMING -> INCOMING
                    pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED -> DISCONNECTED
                    pjsip_inv_state.PJSIP_INV_STATE_CALLING -> CALLING
                    pjsip_inv_state.PJSIP_INV_STATE_EARLY -> EARLY
                    pjsip_inv_state.PJSIP_INV_STATE_CONNECTING -> CONNECTING
                    pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED -> CONFIRMED
                    else -> UNKNOWN
                }
            }
        }
    }
}